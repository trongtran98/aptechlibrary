/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

import dao.BookDAO;
import entities.Book;
import java.util.List;

/**
 *
 * @author Trong Tran
 */
public class BookBean {

    private BookDAO bdao = new BookDAO();

    public List<Book> getAllBooks() {
        return bdao.getAllBooks();
    }

    public List<Book> getBookByName(String keyWord) {
        return bdao.getBookByName(keyWord);
    }

    public Book getBookByRelativeProps(String name, String author, boolean status) {
        return bdao.getBookByRelativeProps(name, author, status);
    }

    public Book getBookByID(String bookID) {
        return bdao.getBookByID(bookID);
    }

    public boolean returnBook(String bookId, String historyId) {
        return bdao.returnBook(bookId, historyId);
    }

    public List<Book> searchBook(String keyword) {
        return bdao.searchBook(keyword);
    }

    public boolean addBook(String bookId, String name, String author) {
        return bdao.addBook(bookId, name, author);
    }

    public boolean editBook(String bookId, String name, String author) {
        return bdao.editBook(bookId, name, author);
    }
}
