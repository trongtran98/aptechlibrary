/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

import dao.IndemnificationDAO;
import entities.Indemnification;
import java.util.List;

/**
 *
 * @author Trong Tran
 */
public class IndemnificationBean {

    IndemnificationDAO idao = new IndemnificationDAO();

    public List<Indemnification> getIndems() {
        return idao.getIndems();
    }

    public boolean addFee(String staffId, String bookId, float fee, int outDate) {
        return idao.addFee(staffId, bookId, fee, outDate);
    }
}
