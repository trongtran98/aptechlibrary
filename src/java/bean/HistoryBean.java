/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

import dao.HistoryDAO;
import entities.History;
import java.sql.Date;
import java.util.List;

/**
 *
 * @author Trong Tran
 */
public class HistoryBean {
    HistoryDAO hdao = new HistoryDAO();
    
    public List<History> getHistoriesByBookID(String bookID){
        return hdao.getHistoriesByBookID(bookID);
    }
    
    public boolean insertHistory(String staffId, String bookID, Date dateLine) {
        return hdao.insertHistory(staffId, bookID, dateLine);
    }
    
     public int getOutDate(String historyId) {
         return hdao.getOutDate(historyId);
     }
}
