/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

import dao.StaffDAO;
import entities.Staff;

/**
 *
 * @author Trong Tran
 */
public class StaffBean {
    private Staff staff;
    StaffDAO staffDAO = new StaffDAO();
    
    public Staff getStaff() {
        return staff;
    }

    public void setStaff(Staff staff) {
        this.staff = staff;
    }
    
    public Staff doLogin(){
        return staffDAO.doLogin(staff);
    }
}
