/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.sql.Date;

/**
 *
 * @author Trong Tran
 */
public class History {
    private int historyid;
    private String staffid;
    private String bookid;
    private String bookname;
    private Date dateborrow;
    private Date deadline;
    private Date datereturn;

    public History(int historyid, String staffid, String bookid,String bookname, Date dateborrow, Date deadline, Date datereturn) {
        this.historyid = historyid;
        this.staffid = staffid;
        this.bookid = bookid;
        this.dateborrow = dateborrow;
        this.deadline = deadline;
        this.datereturn = datereturn;
        this.bookname = bookname;
    }

    public String getBookname() {
        return bookname;
    }

    public void setBookname(String bookname) {
        this.bookname = bookname;
    }

    
    public int getHistoryid() {
        return historyid;
    }

    public void setHistoryid(int historyid) {
        this.historyid = historyid;
    }

    public String getStaffid() {
        return staffid;
    }

    public void setStaffid(String staffid) {
        this.staffid = staffid;
    }

    public String getBookid() {
        return bookid;
    }

    public void setBookid(String bookid) {
        this.bookid = bookid;
    }

    public Date getDateborrow() {
        return dateborrow;
    }

    public void setDateborrow(Date dateborrow) {
        this.dateborrow = dateborrow;
    }

    public Date getDeadline() {
        return deadline;
    }

    public void setDeadline(Date deadline) {
        this.deadline = deadline;
    }

    public Date getDatereturn() {
        return datereturn;
    }

    public void setDatereturn(Date datereturn) {
        this.datereturn = datereturn;
    }
}
