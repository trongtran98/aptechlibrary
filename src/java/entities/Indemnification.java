/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.sql.Date;

/**
 *
 * @author Trong Tran
 */
public class Indemnification {
    private int id;
    private String staffId;
    private String bookId;
    private String bookName;
    private float fee;
    private int outDate;

    public Indemnification(int id, String staffId, String bookId, String bookName, float fee, int outDate) {
        this.id = id;
        this.staffId = staffId;
        this.bookId = bookId;
        this.bookName = bookName;
        this.fee = fee;
        this.outDate = outDate;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

   

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStaffId() {
        return staffId;
    }

    public void setStaffId(String staffId) {
        this.staffId = staffId;
    }

    public String getBookId() {
        return bookId;
    }

    public void setBookId(String bookId) {
        this.bookId = bookId;
    }

    public float getFee() {
        return fee;
    }

    public void setFee(float fee) {
        this.fee = fee;
    }

    public int getOutDate() {
        return outDate;
    }

    public void setOutDate(int outDate) {
        this.outDate = outDate;
    }
    
    
    
}
