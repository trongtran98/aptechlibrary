/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

/**
 *
 * @author Trong Tran
 */
public class Staff {

    private String staffID;
    private String password;
    private String name;
    private boolean rol;

    public Staff(String staffID, String password, String name, boolean rol) {
        this.staffID = staffID;
        this.password = password;
        this.name = name;
        this.rol = rol;
    }
    
    public boolean isRol() {
        return rol;
    }

    public void setRol(boolean rol) {
        this.rol = rol;
    }
    
    
    
    public String getStaffID() {
        return staffID;
    }

    public void setStaffID(String staffID) {
        this.staffID = staffID;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
