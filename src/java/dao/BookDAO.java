/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entities.Book;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Trong Tran
 */
public class BookDAO {

    PreparedStatement ps;
    ResultSet rs;
    ConnectDB conn = new ConnectDB();

    public List<Book> getAllBooks() {
        List<Book> books;
        String sql = "SELECT name,author,status,COUNT(*) AS 'amount' FROM dbo.tblBooks GROUP BY name,author,status";
        try {
            ps = conn.getConnection().prepareStatement(sql);
            rs = ps.executeQuery();
            books = new ArrayList<>();
            while (rs.next()) {
                books.add(new Book(null, rs.getString("name"), rs.getString("author"), rs.getBoolean("status"), rs.getInt("amount")));
            }
            rs.close();
            ps.close();
            return books;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public List<Book> getBookByName(String name) {
        List<Book> books;
        String sql = "SELECT bookid,name,author,status FROM dbo.tblBooks WHERE name = ?";
        try {
            ps = conn.getConnection().prepareStatement(sql);
            ps.setString(1, name);
            rs = ps.executeQuery();
            books = new ArrayList<>();
            while (rs.next()) {
                books.add(new Book(rs.getString("bookid"), rs.getString("name"), rs.getString("author"), rs.getBoolean("status"), 0));
            }
            rs.close();
            ps.close();
            return books;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public Book getBookByRelativeProps(String name, String author, boolean status) {

        Book book = null;
        String sql = "SELECT bookid,name,author,status FROM dbo.tblBooks WHERE name = ? AND author = ? AND status = ?";
        try {
            ps = conn.getConnection().prepareStatement(sql);
            ps.setString(1, name);
            ps.setString(2, author);
            ps.setBoolean(3, status);
            rs = ps.executeQuery();
            if (rs.next()) {
                book = new Book(rs.getString("bookid"), rs.getString("name"), rs.getString("author"), rs.getBoolean("status"), 0);
            }
            rs.close();
            ps.close();
            return book;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public Book getBookByID(String bookid) {
        Book book = null;
        String sql = "SELECT bookid,name,author,status FROM dbo.tblBooks WHERE bookid = ?";
        try {
            ps = conn.getConnection().prepareStatement(sql);
            ps.setString(1, bookid);
            rs = ps.executeQuery();

            if (rs.next()) {
                book = new Book(rs.getString("bookid"), rs.getString("name"), rs.getString("author"), rs.getBoolean("status"), 0);
            }
            rs.close();
            ps.close();
            return book;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return null;
        }

    }

    public boolean returnBook(String bookId, String historyId) {
        String sql = "UPDATE dbo.tblBooks SET status = 1 WHERE bookid =?;"
                + "UPDATE dbo.tblHistory SET datereturn = GETDATE() WHERE historyid = ?";
        int result = 0;
        try {
            ps = conn.getConnection().prepareStatement(sql);
            ps.setString(1, bookId);
            ps.setString(2, historyId);
            result = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return result > 0;
    }

    public List<Book> searchBook(String name) {
        List<Book> books;
        String sql = "SELECT bookid,name,author,status FROM dbo.tblBooks WHERE name LIKE ?";
        try {
            ps = conn.getConnection().prepareStatement(sql);
            ps.setString(1, "%" + name + "%");
            rs = ps.executeQuery();
            books = new ArrayList<>();
            while (rs.next()) {
                books.add(new Book(rs.getString("bookid"), rs.getString("name"), rs.getString("author"), rs.getBoolean("status"), 0));
            }
            rs.close();
            ps.close();
            return books;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public boolean addBook(String bookId, String name, String author) {
        String sql = "INSERT dbo.tblBooks(bookid, name, author) VALUES(?,?,?)";
        int result = 0;
        try {
            ps = conn.getConnection().prepareStatement(sql);
            ps.setString(1, bookId);
            ps.setString(2, name);
            ps.setString(3, author);
            result = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return result > 0;
    }

    public boolean editBook(String bookId, String name, String author) {
        String sql = "UPDATE dbo.tblBooks SET name = ?,author=? WHERE bookid = ?";
        int result = 0;
        try {
            ps = conn.getConnection().prepareStatement(sql);
            ps.setString(1, name);
            ps.setString(2, author);
            ps.setString(3, bookId);
            result = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return result > 0;
    }
}
