/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entities.Indemnification;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Trong Tran
 */
public class IndemnificationDAO {

    PreparedStatement ps;
    ResultSet rs;
    ConnectDB conn = new ConnectDB();

    public boolean addFee(String staffId, String bookId, float fee, int outDate) {
        String sql = "INSERT INTO dbo.tblIndemnification(staffid, bookid, fee, outdate) VALUES  (?,?,?,?)";
        int result = 0;
        try {
            ps = conn.getConnection().prepareStatement(sql);
            ps.setString(1,staffId);
            ps.setString(2, bookId);
            ps.setFloat(3, fee);
            ps.setInt(4, outDate);
            result = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }
        return result>0;
    }
    
    public List<Indemnification> getIndems(){
        List<Indemnification> list = null;
        String sql = "SELECT id,staffid,bookid,(SELECT name FROM dbo.tblBooks WHERE dbo.tblBooks.bookid = dbo.tblIndemnification.bookid) AS bookname,fee,outdate FROM dbo.tblIndemnification ";
        try {
            ps = conn.getConnection().prepareStatement(sql);
            rs = ps.executeQuery();
            list= new ArrayList<>();
            while(rs.next()){
                list.add(new Indemnification(rs.getInt("id"),rs.getString("staffid"),rs.getString("bookid"),rs.getString("bookname"),rs.getFloat("fee"),rs.getInt("outdate")));
            }
            
        } catch (SQLException ex) {
            ex.printStackTrace();
            return null;
        }
        return list;
    }
}
