/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Trong Tran
 */
public class ConnectDB {

    private final String DRIVER;
    private final String URL;
    private final String DATABASE;
    private final String USERNAME;
    private final String PASSWORD;

    public ConnectDB() {
        Properties properties = new Properties();
        ClassLoader loader = this.getClass().getClassLoader();
        try {
            InputStream input = loader.getResourceAsStream("DBConfig/databaseConf.properties");
            properties.load(input);
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        System.out.println(properties.getProperty("driver"));
        this.DRIVER = properties.getProperty("driver");
        this.URL = properties.getProperty("url");
        this.DATABASE = properties.getProperty("database");
        this.USERNAME = properties.getProperty("username");
        this.PASSWORD = properties.getProperty("password");
    }

    public Connection getConnection() {
        Connection conn = null;
        try {
            Class.forName(DRIVER);
            conn = DriverManager.getConnection(URL + "database=" + DATABASE, USERNAME, PASSWORD);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ConnectDB.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(ConnectDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        return conn;
    }

}
