/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entities.History;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Trong Tran
 */
public class HistoryDAO {

    PreparedStatement ps;
    ResultSet rs;
    ConnectDB conn = new ConnectDB();

    public List<History> getHistoriesByBookID(String bookID) {
        List<History> histories;
        String sql = " SELECT historyid,staffid,bookid,(SELECT dbo.tblBooks.name FROM dbo.tblBooks WHERE dbo.tblBooks.bookid = ?) AS bookname,dateborrow,deadline,datereturn FROM dbo.tblHistory WHERE bookid = ?";
        try {
            ps = conn.getConnection().prepareStatement(sql);
            ps.setString(1, bookID);
            ps.setString(2, bookID);
            rs = ps.executeQuery();
            histories = new ArrayList<>();
            while (rs.next()) {
                histories.add(new History(rs.getInt("historyid"), rs.getString("staffid"), rs.getString("bookid"),rs.getString("bookname"), rs.getDate("dateborrow"), rs.getDate("deadline"), rs.getDate("datereturn")));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            return null;
        }
        return histories;
    }

    public boolean insertHistory(String staffId, String bookID, Date dateLine) {
        String sql = "INSERT dbo.tblHistory(staffid,bookid,dateborrow,deadline) VALUES (?,?,GETDATE(),?);"
                + "UPDATE dbo.tblBooks SET status = 0 WHERE bookid =?";
        int result = 0;
        try {
            ps = conn.getConnection().prepareStatement(sql);
            ps.setString(1, staffId);
            ps.setString(2, bookID);
            ps.setDate(3, dateLine);
            ps.setString(4, bookID);
            result = ps.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(HistoryDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result > 0;
    }

    public int getOutDate(String historyId) {
        String sql = "SELECT DATEDIFF(DAY,(SELECT deadline FROM dbo.tblHistory WHERE historyid = ?),(SELECT datereturn FROM dbo.tblHistory WHERE historyid = ?)) AS outdate";
        try {
            ps = conn.getConnection().prepareStatement(sql);
            ps.setString(1, historyId);
            ps.setString(2, historyId);
            rs = ps.executeQuery();
            if(rs.next()){
                return rs.getInt("outdate");
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            return -211198;
        }
        System.out.println("Can not get outdate");
        return -211198;
    }
    
}
