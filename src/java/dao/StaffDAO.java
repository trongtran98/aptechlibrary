/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entities.Staff;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Trong Tran
 */
public class StaffDAO {

    PreparedStatement ps;
    ResultSet rs;
    ConnectDB conn = new ConnectDB();

    public Staff doLogin(Staff staff) {
        String sql = "SELECT * FROM tblStaffs WHERE staffid = ? and password =?";
        try {
            ps = conn.getConnection().prepareStatement(sql);
            ps.setString(1, staff.getStaffID());
            ps.setString(2, staff.getPassword());
            rs = ps.executeQuery();
            if (rs.next()) {
                return new Staff(rs.getString("staffid"),null,rs.getString("name"),rs.getBoolean("rol"));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            return null;
        }
        return null;
    }
}
