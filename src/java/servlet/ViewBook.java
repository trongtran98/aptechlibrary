/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import bean.BookBean;
import entities.Book;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Trong Tran
 */
public class ViewBook extends HttpServlet {

   
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String type = request.getParameter("type");
        BookBean bean = new BookBean();
        Book book;
        if(type.equals("props")){
            
            String name = request.getParameter("name");
            String author = request.getParameter("author");
            boolean status = Boolean.parseBoolean(request.getParameter("status"));
            book = bean.getBookByRelativeProps(name, author, status);
            if(book!=null){
                request.setAttribute("book", book);
                request.getRequestDispatcher("details.jsp").forward(request, response);
            }
        }else if(type.equals("id")){
            String bookID = request.getParameter("bookID");
            book = bean.getBookByID(bookID);
            if(book!=null){
                request.setAttribute("book", book);
                request.getRequestDispatcher("details.jsp").forward(request, response);
            }
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
