/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import bean.BookBean;
import entities.Book;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Trong Tran
 */
public class SearchServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        BookBean bean = new BookBean();
        String type = request.getParameter("type");
        if (type != null) {
            if (type.equals("edit")) {
                String bookId = request.getParameter("bookID");
                Book book = bean.getBookByID(bookId);
                request.setAttribute("book", book);
                request.getRequestDispatcher("editBook.jsp").forward(request, response);
            }
        } else {
            String keyWord = request.getParameter("keyword");
            List<Book> books = bean.searchBook(keyWord);
            request.setAttribute("search", true);
            request.setAttribute("booksSearch", books);
            request.getRequestDispatcher("index.jsp").forward(request, response);
        }

    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
