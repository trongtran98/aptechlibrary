/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import bean.StaffBean;
import dao.StaffDAO;
import entities.Staff;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Properties;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Trong Tran
 */
public class LoginServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");

        if (username != null && password != null) {
            StaffBean bean = new StaffBean();
            Staff staff = new Staff(username,password,null,false);
            bean.setStaff(staff);
            
            Staff sResult = bean.doLogin();
            if (sResult != null) {
                HttpSession session = request.getSession();
                session.setAttribute("userid", sResult.getStaffID());
                session.setAttribute("name", sResult.getName());
                session.setAttribute("rol", sResult.isRol());
                response.sendRedirect("index.jsp");
            } else {
                request.setAttribute("error", true);
                request.getRequestDispatcher("login.jsp").forward(request, response);
            }
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
