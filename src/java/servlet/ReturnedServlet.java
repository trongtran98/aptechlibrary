/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import bean.BookBean;
import bean.HistoryBean;
import bean.IndemnificationBean;
import dao.BookDAO;
import dao.HistoryDAO;
import dao.IndemnificationDAO;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Trong Tran
 */
public class ReturnedServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String bookId = request.getParameter("bookId");
        String historyId = request.getParameter("historyId");
        HttpSession session = request.getSession();
        String staffId = (String) session.getAttribute("userid");
        
        HistoryBean hb = new HistoryBean();
        BookBean bb = new BookBean();
        IndemnificationBean ib = new IndemnificationBean();
        if(bb.returnBook(bookId, historyId)){
            int outDate = hb.getOutDate(historyId);
            if(outDate>0){
                if(ib.addFee(staffId,bookId, outDate*5000, outDate)){
                    response.sendRedirect("HistoryServlet?bookID="+bookId);
                }else{
                    response.sendRedirect("HistoryServlet?bookID="+bookId);
                }
            }else{
                response.sendRedirect("HistoryServlet?bookID="+bookId);
            }
        }else{
            response.sendRedirect("error.jsp?msg=Error!");
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
