/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import dao.HistoryDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Trong Tran
 */
public class BorrowServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String bookID = request.getParameter("bookID");
        Date deadLine = Date.valueOf(request.getParameter("deadLine"));
        Date dateBorrow = Date.valueOf(request.getParameter("dateBorrow"));
        HttpSession session = request.getSession();
        String userId = (String) session.getAttribute("userid");
        
        HistoryDAO hdao = new HistoryDAO();
        
        if(deadLine.compareTo(dateBorrow)>=1){
            if(hdao.insertHistory(userId,bookID,deadLine)){
                response.sendRedirect("HistoryServlet?bookID="+bookID);
            }else{
                response.sendRedirect("error.jsp?error=Error!");
            }
        }else{
            response.sendRedirect("error.jsp?error=Error!");
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
