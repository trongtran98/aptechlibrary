<%-- 
    Document   : details
    Created on : Apr 2, 2018, 10:07:30 PM
    Author     : Trong Tran
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="i18n.text"/>
<c:if test="${sessionScope.userid == null}">
    <c:redirect url="login.jsp"/>
</c:if>
<!DOCTYPE html>
<html lang="${language}">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Book Details Page</title>
    </head>
    <body>
        <div class="container">
            <%@include file="master.jsp" %>

            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <td>
                            <a href="#">
                                <fmt:message key="title.book.id"/>                            
                            </a>
                        </td>
                        <td>
                            <a href="#">
                                <fmt:message key="title.book.name"/>    
                            </a>
                        </td>
                        <td>
                            <a href="#">
                                <fmt:message key="title.book.author"/>  
                            </a>
                        </td>
                        <td>
                            <a href="#">
                                <fmt:message key="title.book.status"/>  
                            </a>
                        </td>
                        <td>
                            <a href="#">
                                <fmt:message key="title.book.amount"/>  
                            </a>
                        </td>
                        <c:if test="${book.status eq true}">
                            <td>
                                <a href="#">
                                    Mượn sách  
                                </a>
                            </td>
                        </c:if>
                        <c:if test="${rol eq true}">
                            <td colspan="2">
                                <a href="#">
                                    Thao tác
                                </a>
                            </td>
                        </c:if>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>${book.bookID}</td>
                        <td>${book.name}</td>
                        <td>${book.author}</td>
                        <c:if test="${book.status eq true}">
                            <td><fmt:message key="infomation.book.statustrue"/></td>
                        </c:if >
                        <c:if test="${book.status ne true}">
                            <td><fmt:message key="infomation.book.statusfalse"/></td>
                        </c:if >
                        <td>1</td>
                        <c:if test="${book.status eq true}">
                            <td><button onclick="showDiv()"><fmt:message key="page.button.borrow"/></button></td>
                        </c:if>
                        <c:if test="${rol eq true}">
                            <td><a href="HistoryServlet?bookID=${book.bookID}"><fmt:message key="page.button.history"/></a></td>
                            <td><a href="SearchServlet?bookID=${book.bookID}&type=edit"><fmt:message key="page.button.edit"/></a></td>
                        </c:if>
                    </tr>
                </tbody>
            </table>
            <div style="display: none; margin-top: 20px;" id="borrowForm">
                <form action="BorrowServlet" method="post">
                    <input hidden name="bookID" value="${book.bookID}"/><br/>

                    <label for="book-name"><fmt:message key="title.book.name"/></label>
                    <input readonly style="width: 35%; height: 20px;" type="text" class="form-control" id="book-name" value="${book.name}"/>
                    <br/>
                    <label for="author"><fmt:message key="title.book.author"/></label>
                    <input readonly style="width: 35%; height: 20px;" type="text" class="form-control" id="author" value="${book.author}"/>                  

                    <input hidden name="dateBorrow" id="dateborrow" type="date"/><br/>

                    <label for="deadLine"><fmt:message key="title.history.duedate"/></label>
                    <input readonly name="deadLine" style="width: 35%; height: 20px;" type="date" class="form-control" id="deadLine"/>                  
                    <br/>
                    <button style="color: #fff;
                            background-color: #007bff;
                            border-color: #007bff;padding: 5px 10px;" type="submit" class="btn"><fmt:message key="page.button.submit"/></button>
                    <br/><br/><i><fmt:message key="page.note.borrow"/></i><br/>
                </form>
            </div>
            <p class="text-center text-muted">
                <a href="index.jsp"><fmt:message key="page.button.home"/></a>
            </p>
        </div>
        <script type="text/javascript">
            var today = new Date();
            function showDiv() {
                document.getElementById("borrowForm").style.display = 'block';
                showToday(today);
                showDeadline(today);
            }
            function showToday(today) {
                var dd = today.getDate();
                var mm = today.getMonth() + 1; //January is 0!

                var yyyy = today.getFullYear();
                if (dd < 10) {
                    dd = '0' + dd;
                }
                if (mm < 10) {
                    mm = '0' + mm;
                }
                var today = yyyy + '-' + mm + '-' + dd;
                document.getElementById("dateborrow").value = today;
            }
            function showDeadline(today) {
                var deadLine = new Date(today);
                deadLine.setDate(deadLine.getDate() + 7);
                var dd = deadLine.getDate();
                var mm = deadLine.getMonth() + 1;
                var yyyy = deadLine.getFullYear();
                if (dd < 10) {
                    dd = '0' + dd;
                }
                if (mm < 10) {
                    mm = '0' + mm;
                }
                var someFormattedDate = yyyy + '-' + mm + '-' + dd;
                console.log(someFormattedDate);
                document.getElementById("deadLine").value = someFormattedDate;
            }
        </script>

    </body>
</html>
