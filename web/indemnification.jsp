<%-- 
    Document   : indemnification
    Created on : Apr 4, 2018, 11:55:55 PM
    Author     : Trong Tran
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="i18n.text"/>
<c:if test="${sessionScope.userid == null}">
    <c:redirect url="login.jsp"/>
</c:if>
<!DOCTYPE html>
<html lang="${language}">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Indemnification Page</title>
    </head>
    <body>
        <div class="container">
            <jsp:useBean id="indems" class="bean.IndemnificationBean"/>
            <%@include file="master.jsp" %>      
            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <td>
                            <a href="#">
                                <fmt:message key="title.indemnification.id"/>                         
                            </a>
                        </td>
                        <td>
                            <a href="#">
                                <fmt:message key="title.indemnification.staffid"/>
                            </a>
                        </td>
                        <td>
                            <a href="#">
                                <fmt:message key="title.indemnification.bookid"/>
                            </a>
                        </td>
                        <td>
                            <a href="#">
                                <fmt:message key="title.indemnification.bookname"/>
                            </a>
                        </td>
                        <td>
                            <a href="#">
                                <fmt:message key="title.indemnification.fee"/>
                            </a>
                        </td>
                        <td>
                            <a href="#">
                                <fmt:message key="title.indemnification.overduedays"/>
                            </a>
                        </td>

                    </tr>
                </thead>
                <tbody>
                    <c:forEach items="${indems.indems}" var="i">
                        <tr>
                            <td>${i.id}</td>
                            <td>${i.staffId}</td>
                            <td>${i.bookId}</td>
                            <td>${i.bookName}</td>
                            <td><fmt:formatNumber value = "${i.fee}" type = "currency"/> (VNĐ)</td>
                            <td>${i.outDate}</td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
              <p class="text-center text-muted">
                <a href="index.jsp"><fmt:message key="page.button.home"/></a>
            </p>

        </div>
    </body>
</html>
