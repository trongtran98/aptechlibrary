<%-- 
    Document   : history
    Created on : Apr 2, 2018, 11:32:55 PM
    Author     : Trong Tran
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="i18n.text"/>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<c:if test="${sessionScope.userid == null}">
    <c:redirect url="login.jsp"/>
</c:if>
<!DOCTYPE html>
<html lang="${language}">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Book History Page</title>
    </head>
    <body>
        <div class="container">
            <%@include file="master.jsp" %>
            <c:if test="${fn:length(histories) > 0}">
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <td>
                                <a href="#">
                                     <fmt:message key="title.history.id"/>                                         
                                </a>
                            </td>
                            <td>
                                <a href="#">
                                    <fmt:message key="title.history.staffid"/>          
                                </a>
                            </td>
                             <td>
                                <a href="#">
                                    <fmt:message key="title.history.bookname"/>          
                                </a>
                            </td>
                            <td>
                                <a href="#">
                                    <fmt:message key="title.history.indate"/> 
                                </a>
                            </td>
                            <td>
                                <a href="#">
                                    <fmt:message key="title.history.duedate"/> 
                                </a>
                            </td>
                            <td>
                                <a href="#">
                                    <fmt:message key="title.history.outdate"/> 
                                </a>
                            </td>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${histories}" var="h">
                            <tr>
                                <td>${h.historyid}</td>
                                <td>${h.staffid}</td>
                                <td>${h.bookname}</td>
                                <td><fmt:formatDate value="${h.dateborrow}" pattern="dd-MM-yyyy"/></td>
                                <td><fmt:formatDate value="${h.deadline}" pattern="dd-MM-yyyy"/></td>
                                <c:if test="${not empty h.datereturn}">
                                    <td><fmt:formatDate value="${h.datereturn}" pattern="dd-MM-yyyy"/></td>
                                </c:if>
                                <c:if test="${empty h.datereturn}">
                                    <td style="color: red;"><fmt:message key="page.lable.returned"/></td>
                                </c:if>
                                <c:if test="${rol eq true and empty h.datereturn}">
                                    <td><a href="ReturnedServlet?historyId=${h.historyid}&bookId=${h.bookid}"><fmt:message key="page.button.returned"/></td>
                                </c:if>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </c:if>
            <c:if test="${fn:length(histories) <= 0}">
                <h4><fmt:message key="page.note.nohistory"/></h4>
            </c:if>
              <p class="text-center text-muted">
                <a href="index.jsp"><fmt:message key="page.button.home"/></a>
            </p>

        </div>
    </body>
</html>
