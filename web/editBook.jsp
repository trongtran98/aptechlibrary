<%-- 
    Document   : editBook
    Created on : Apr 5, 2018, 12:58:18 AM
    Author     : Trong Tran
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="i18n.text"/>
<c:if test="${sessionScope.userid == null}">
    <c:redirect url="login.jsp"/>
</c:if>
<!DOCTYPE html>
<html lang="${language}">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Edit Page</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    </head>
    <body>
       
        <div class="container">
            <%@include file="master.jsp" %>
            <form action="AddServlet" method="post">
                <div class="form-group">
                    <label style="font-weight: 600;" for="bookId"> <fmt:message key="title.book.id"/></label>
                    <input required readonly type="text" name="bookId" class="form-control" id="bookId" value="${book.bookID}">

                </div>
                <div class="form-group">
                    <label style="font-weight: 600;" for="name"> <fmt:message key="title.book.name"/></label>
                    <input required type="text" name="name" class="form-control" id="name" value="${book.name}">
                </div>

                <div class="form-group">
                    <label style="font-weight: 600;" for="author"> <fmt:message key="title.book.author"/></label>
                    <input required type="text" name="author" class="form-control" id="author" value="${book.author}">
                </div>

                <button type="submit" class="btn btn-primary"><fmt:message key="page.button.updatebook"/></button>
            </form>
        </div>
    </body>
</html>
