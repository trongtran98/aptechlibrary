<%-- 
    Document   : master
    Created on : Apr 4, 2018, 4:33:08 PM
    Author     : Trong Tran
--%>

<link rel='stylesheet prefetch' href='http://maxcdn.bootstrapcdn.com/bootswatch/3.2.0/sandstone/bootstrap.min.css'>
<link rel='stylesheet prefetch' href='http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css'>
<link rel="stylesheet" href="css/style.css">

<div class="alert alert-info">
    <p><fmt:message key="infomation.user.id"/>: <b>${sessionScope.userid}</b></p>
<p><fmt:message key="infomation.user.name"/>: <b>${sessionScope.name}</b></p>
<c:if test="${rol eq true}">
    <p><fmt:message key="infomation.user.role"/>: <b style="color: indianred;"><fmt:message key="infomation.user.type.admin"/></b></p>
</c:if>
<c:if test="${rol eq false}">
    <p><fmt:message key="infomation.user.role"/>: <fmt:message key="infomation.user.type.staff"/></p>
</c:if>

</div>
<div style="width: 100%;display: inline-block; height: auto; margin-bottom: 20px;">

    <form style="float:left; margin: 0px 3px;" action="LogoutServlet">
        <button type="submit" class="btn"><fmt:message key="master.button.logout"/></button>
    </form>
    <c:if test="${rol eq true}">
        <button onclick="redirect('addBook')" class="btn"><fmt:message key="master.button.newbook"/></button>
        <button onclick="redirect('indemnification')" class="btn"><fmt:message key="master.button.indemnification"/></button>
    </c:if>

</div>
<form id="searchform" action="SearchServlet">
    <div class="form-group">
        <div class="input-group">
            <div onclick="submitForm()" class="input-group-addon">
                <i class="fa fa-search"></i>   
            </div>
            <input required type="text" id="keyword" name="keyword" class="form-control" placeholder="Enter text...">

        </div>      
    </div>
</form>

<script type="text/javascript">
    function submitForm() {
        var keyword = document.getElementById("keyword").value;
        console.log(keyword);
        if (keyword !== "") {
            document.getElementById('searchform').submit();
        } else {
            alert("Field can't be empty");
        }
    }

    function redirect(page) {
        if (page === 'addBook') {
            window.location.replace("addBook.jsp");
        } else if (page === 'indemnification') {
            window.location.replace("indemnification.jsp");
        }
    }
</script>