<%-- 
    Document   : index.jsp
    Created on : Apr 2, 2018, 8:31:00 PM
    Author     : Trong Tran
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="i18n.text"/>
<c:if test="${sessionScope.userid == null}">
    <c:redirect url="login.jsp"/>
</c:if>
<!DOCTYPE html>
<html lang="${language}">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Home Page</title>
    </head>
    <body>
        <div class="container">
            <%@include file="master.jsp" %>

            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <td>
                            <a href="#">
                               <fmt:message key="title.book.name"/>                         
                            </a>
                        </td>
                        <td>
                            <a href="#">
                                <fmt:message key="title.book.author"/>
                            </a>
                        </td>
                        <td>
                            <a href="#">
                                <fmt:message key="title.book.status"/>
                            </a>
                        </td>
                        <td>
                            <a href="#">
                                <fmt:message key="title.book.amount"/>
                            </a>
                        </td>
                    </tr>
                </thead>

                <tbody>
                    <c:if test="${search eq true}">
                        <c:forEach items="${booksSearch}" var="b">
                            <tr>                    
                                <td>${b.name}</td>
                                <td>${b.author}</td>
                                <c:if test="${b.status eq true}">
                                    <td><fmt:message key="infomation.book.statustrue"/></td>
                                </c:if >
                                <c:if test="${b.status ne true}">
                                    <td><fmt:message key="infomation.book.statusfalse"/></td>
                                </c:if >
                                <td>1</td>         
                                <td><a href="ViewBook?type=id&bookID=${b.bookID}"><fmt:message key="page.button.details"/></a></td>
                            </tr>
                        </c:forEach>
                    </c:if>
                    <c:if test="${empty search}">
                        <jsp:useBean id="books" scope="request" class="bean.BookBean"/>
                        <c:if test="${param.type ne 'view'}">
                            <c:forEach items="${books.allBooks}" var="b">
                                <tr>                    
                                    <td>${b.name}</td>
                                    <td>${b.author}</td>
                                    <c:if test="${b.status eq true}">
                                        <td><fmt:message key="infomation.book.statustrue"/></td>
                                    </c:if >
                                    <c:if test="${b.status ne true}">
                                        <td><fmt:message key="infomation.book.statusfalse"/></td>
                                    </c:if >
                                    <td>${b.amount}</td>
                                    <c:if test="${b.amount > 1}">
                                        <td><a href="index.jsp?type=view&name=${b.name}"><fmt:message key="page.button.view"/></a></td>
                                    </c:if>
                                    <c:if test="${b.amount == 1}">
                                        <td><a href="ViewBook?type=props&name=${b.name}&author=${b.author}&status=${b.status}"><fmt:message key="page.button.details"/></a></td>
                                    </c:if>
                                </tr>
                            </c:forEach>
                        </c:if>

                        <c:if test="${param.type eq 'view'}">
                            <c:forEach items="${books.getBookByName(param.name)}" var="b">
                                <tr>                    
                                    <td>${b.name}</td>
                                    <td>${b.author}</td>
                                    <c:if test="${b.status eq true}">
                                        <td><fmt:message key="infomation.book.statustrue"/></td>
                                    </c:if >
                                    <c:if test="${b.status ne true}">
                                        <td><fmt:message key="infomation.book.statusfalse"/></td>
                                    </c:if >
                                    <td>1</td>         
                                    <td><a href="ViewBook?type=id&bookID=${b.bookID}"><fmt:message key="page.button.view"/></a></td>
                                </tr>
                            </c:forEach>
                        </c:if>

                    </c:if>

                </tbody>

            </table>

            <p class="text-center text-muted">
                <a href="index.jsp"><fmt:message key="page.button.home"/></a>
            </p>
        </div>
    </div>
</body>
</html>
